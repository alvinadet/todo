const simpanDOM = document.getElementById('simpan')
const inputDOM = document.getElementById('input')
const listDOM = document.getElementById('list')
let Data = []

localData = JSON.parse(localStorage.getItem("Daftar"))
if(localData != null){
    Data = localData
}
render()

function Tambah (){
    const inputBaru = inputDOM.value
    Data.push(inputBaru)
    
    inputDOM.value = ""
    localStorage.setItem("Daftar", JSON.stringify(Data))
   
    
}

function render(){
    let Tampil = ""
    for(let i = 0; i < Data.length; i++){
        Tampil +=  `
        <li class="m-2"><div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Note</h5>
          <h6 class="card-subtitle mb-2 text-muted">Yang Ingin Diingat</h6>
          <p class="card-text">
         ${Data[i]}
          </p>
          <div class="row">
            <div class="col-md-6">
              <button type="button" class="btn btn-outline-danger btn btn-block" onclick="hapus(${i})">Hapus</button>
            </div>
            <div class="col-md-6">
              <button type="button" class="btn btn-outline-success btn btn-block">Edit</button>
            </div>
          </div>
        </div>
        </li>
        `
    }
    listDOM.innerHTML = Tampil
    
}
function hapus(id){
    Data.splice(id, 1);
    localStorage.setItem("Daftar",JSON.stringify(Data));
    render()
}

